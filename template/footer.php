			</div>
			 <script>window.jQuery || document.write('<script src="assets/js/jquery-slim.min.js"><\/script>')</script>
			 <script src="assets/js/jquery-1.9.1.min.js" type="text/javascript"></script>
			 <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

			<script type="text/javascript">
				$(document).ready(function(){
					showRecords();
				});
				function showRecords(){
					$.ajax({
						url:'action.php',
						type:'POST',
						data:{action: 'showRecords'},
						dataType:'html',
						success:function(records){
							$('#Records').html(records);
						}
					});
				}
				$('#addForm').submit(function(e){
						e.preventDefault();
						$.ajax({
							url:'action.php',
						type:'POST',
						dataType:'html',
						data:$(this).serialize(),
						success:function(){
							$('#addModal').modal('hide');
							showRecords();
						},
						error:function(xhr,ajaxOptions,thrownError)
						{
							alert("Error: " + thrownError);
						}
				});
					});
			</script>
	</body>
</html>
