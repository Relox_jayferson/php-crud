<?php
     
    include'config.php';
    $page_title = 'Home';
    include 'template/header.php';

    ?>

        <div class ="row">
            <div class="col-md-2"> 
                <button class="btn btn-lg btn-primary" type="button" data-target="#addModal" data-toggle="modal">Add Record</button>
            </div>
        </div>

        <table class="table table-striped"> 
                <thead>
                    <tr>
                            <th>    ID </th>
                            <th>    Name </th>
                            <th>    Age </th>
                            <th>    Address </th>
                            <th>    Action </th>            
                    </tr>
                </thead>
                        <tbody id="Records"> </tbody>
        </table>

        <div id="addModal" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Record </h4>
                    <button type="button" class="close" data-dismiss="modal">x</button>

                </div>
        <form id="addForm">
                <div class="modal-body">
                    <div class ="row">
                        <div class="col-md-4">
                             <label>Name: </label>
                                <input type="text" name="name" class="form-control" placeholder="e.g. Jayferson I. Relox"/> 
                        </div>
                    </div>
                <div class ="row">
                    <div class="col-md-4">
                            <label>Age: </label>
                                <input type="number" name="age" class="form-control" placeholder="e.g. 20"/>
                    </div>
                </div>
                <div class ="row">
                    <div class="col-md-4">
                            <label>Address: </label>
                                <textarea name="address" class="form-control" placeholder="e.g.Proper Bansud"/> </textarea> 
                    </div>
                </div>
                </div>      
                <input type="hidden" name="action" value="EditRecord"/> 
                        <button class="btn btn-primary" type="edit"> Edit</button>

                    <input type="hidden" name="action" value="Deleterecord"/> 
                        <button class="btn btn-danger" type="delete"> Delete</button>    
                        
                <div class="modal-footer">
                    <input type="hidden" name="action" value="AddRecord"/> 
                        <button class="btn btn-primary" type="submit"> Add</button>
                        <button class="btn btn-danger" type="submit" data-dismiss="modal">Close</button>

                </div>
        </form>
            </div>
            </div>
        </div>


       <?php

    include 'template/footer.php';
    ?>


    