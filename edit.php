<?php
	include 'config.php';
	$page_title ='edit';
	include 'template/header.php';

	$id = $_GET['id'];

$stmt = $db_con->prepare("SELECT * FROM students WHERE id = :id");
$stmt->bindparam(':id', $id);
$stmt->execute();

$row = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<div class= "card box-shadow">
		<div class="card-header">
	<h4>Edit Record</h4>
	</div>
	<div class="card-body">
<form action="action.php" method="post">
				<div class = "row">
				<div class="col-md-4">
					<label>name:</label>
					<input type="text" name="name" class="form-control" value = "<?php echo $row['name']; ?>" />
				</div>
				</div>

				<div class = "row">
				<div class="col-md-4">
					<label> age:</label>
					<input type="number" name="age" class="form-control" value = "<?php echo $row['age']; ?>"/>
				</div>
				</div>



				<div class = "row">
				<div class="col-md-4">
					<label>address:</label>
					<textarea name="address" class="form-control">
					<?php echo $row['address']; ?>
				</textarea>
								</div>
				</div>
				
			<input type ="hidden" name ="action" value="edit">
			<input type="hidden" name="id" value = "<?php echo $row['id']; ?>" >
			<button type="submit" class="btn btn-primary">
				Edit</button>


			</form>
		</div>
</div>

<?php


include 'template/footer.php';
?>