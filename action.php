<?php

include 'config.php';

if (isset($_POST['action']) && !empty($_POST['action'])){


		$action = $_POST['action'];

	switch($action){
		case 'showRecords':
		showRecords();
		break;
		case 'AddRecord':
		AddRecord();
		break;
		case 'Deleterecord':
		Deleterecord();
		break;
		case 'EditRecord':
		EditRecord();
		break;
		default:
	}
}

	function showRecords() {
		include 'config.php';
		try{

			$stmt=$db_con->prepare("SELECT * FROM students");
			$stmt->execute();
			while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
				?>
				<tr>
				<td><?php echo $row['id']?></td>
				<td><?php echo $row['name']?></td>	
				<td><?php echo $row['age']?></td>
				<td><?php echo $row['address']?></td>
				<td>


					 <input type="hidden" name="action" value="EditRecord"/> 
                        <button class="btn btn-primary" type="edit"> Edit</button>

                    <input type="hidden" name="action" value="Deleterecord"/> 
                        <button class="btn btn-danger" type="delete"> Delete</button>    

				</td>
				</tr>
				<?php
			}
		return true;
		} catch (PDOException $ex){
			echo $ex->getMessage();
			return false;
		}
	}

		function AddRecord() {
		include 'config.php';
		try{ 
			$name = $_POST['name'];
			$age = $_POST['age'];
			$address = $_POST['address'];

			$stmt = $db_con->prepare("INSERT INTO students (name,age,address) VALUES (:name, :age,:address)");
			$stmt->bindparam(":name", $name);
			$stmt->bindparam(':age', $age);
			$stmt->bindparam(":address", $address);
			$stmt->execute();

			return true;

		} catch (PDOException $ex){
			echo $ex->getMessage();
			return false;
		}
	}


	function Deleterecord() {
		
		include 'config.php';

		try {
	

		$id = $_GET['id'];

		$stmt = $db_con->prepare("DELETE FROM students WHERE :id=id");
		$stmt->bindparam(':id', $id);
		$stmt->execute();

			return true;

		} catch (PDOException $ex){
			echo $ex->getMessage();
			return false;
		}
	}


	function EditRecord() {
		
		include 'config.php';

		try {
	$page_title ='edit';
	include 'template/header.php';

	$id = $_GET['id'];

$stmt = $db_con->prepare("SELECT * FROM students WHERE id = :id");
$stmt->bindparam(':id', $id);
$stmt->execute();

$row = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<div class= "card box-shadow">
		<div class="card-header">
	<h4>Edit Record</h4>
	</div>
	<div class="card-body">
<form action="action.php" method="post">
				<div class = "row">
				<div class="col-md-4">
					<label>name:</label>
					<input type="text" name="name" class="form-control" value = "<?php echo $row['name']; ?>" />
				</div>
				</div>

				<div class = "row">
				<div class="col-md-4">
					<label> age:</label>
					<input type="number" name="age" class="form-control" value = "<?php echo $row['age']; ?>"/>
				</div>
				</div>



				<div class = "row">
				<div class="col-md-4">
					<label>address:</label>
					<textarea name="address" class="form-control">
					<?php echo $row['address']; ?>
				</textarea>
								</div>
				</div>
				
			<input type ="hidden" name ="action" value="edit">
			<input type="hidden" name="id" value = "<?php echo $row['id']; ?>" >
			<button type="submit" class="btn btn-primary">
				Edit</button>


			</form>
		</div>
</div>

<?php

			return true;

		} catch (PDOException $ex){
			echo $ex->getMessage();
			return false;
		}
	}
?>