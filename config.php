
<?php

	$db_host = "localhost";
	$db_user = "root";
	$db_pw = "";
	$db_name = "mydbase";

	try {

      $db_con = new PDO("mysql:host={$db_host};dbname={$db_name}",$db_user,$db_pw);
      $db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
	} catch(PDOException $ex) {
		echo $ex->getMessage();
	}

	?>

